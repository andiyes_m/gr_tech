class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  def authenticate_user_from_email_and_token
    authn_token = session[:authn_token]

    user = User.find_by(email: session[:email])
    redirect_to "/login.html?notice=Login First" if user.nil? || user.authn_token != session[:authn_token]
  end

  def authorize_user_menu
    redirect_to "/home.html?notice=You are not authorized" if session[:email] == "user@grtech.com.my"
  end
end
