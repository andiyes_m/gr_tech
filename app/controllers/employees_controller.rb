class EmployeesController < ApplicationController
  before_action :set_employee, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user_from_email_and_token
  before_action :authorize_user_menu

  # GET /employees
  # GET /employees.json
  def index
    page = params[:start] || 0
    per_page = params[:length] || 10
    @employees = Employee.paginate(page: (page.to_i == 0? 1 : (page.to_i/per_page.to_i) + 1), per_page: per_page.to_i)
    @employees = @employees.map{ |m| m.attributes.merge({full_name: m.get_full_name, company: {id: m.company_id, name: m.try(:company).try(:name)}})}
    respond_to do |format|
      format.html
      format.json{
        render :json => {draw: params[:draw], recordsTotal: Employee.all.count, recordsFiltered: Employee.all.count, data: @employees}
      }
    end
  end

  # GET /employees/1
  # GET /employees/1.json
  def show
  end

  # GET /employees/new
  def new
    @employee = Employee.new
  end

  # GET /employees/1/edit
  def edit
  end

  # POST /employees
  # POST /employees.json
  def create
    @employee = Employee.new(employee_params)

    respond_to do |format|
      if @employee.save
        format.html { redirect_to @employee, notice: 'Employee was successfully created.' }
        format.json { render :show, status: :created, location: @employee }
      else
        format.html { render :new }
        format.json { render json: @employee.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /employees/1
  # PATCH/PUT /employees/1.json
  def update
    respond_to do |format|
      if @employee.update(employee_params)
        format.html { redirect_to @employee, notice: 'Employee was successfully updated.' }
        format.json { render :show, status: :ok, location: @employee }
      else
        format.html { render :edit }
        format.json { render json: @employee.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /employees/1
  # DELETE /employees/1.json
  def destroy
    @employee.destroy
    respond_to do |format|
      format.html { redirect_to employees_url, notice: 'Employee was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_employee
      @employee = Employee.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def employee_params
      params.require(:employee).permit(:id, :first_name, :last_name, :company_id, :email, :phone)
    end
end
