class SessionsController < ApplicationController
  before_action :authenticate_user_from_email_and_token, only: [:home]
  def login_process
    user = User.find_by(email: params[:user][:email])
    redirect_to "/login.html?notice=Invalid login or password" unless user.present?

    password_valid = (BCrypt::Password.new(user.encrypted_password) == params[:user][:password])

    redirect_to "/login.html?notice=Invalid login or password" unless password_valid

    @email = user.email
    @authn_token = SecureRandom.hex
    user.update_column(:authn_token, @authn_token)

    session[:email] = @email
    session[:authn_token] = @authn_token

    redirect_to "/home.html"
  end

  def login
    @user = User.new
  end

  def logout
    session[:email] = nil
    session[:authn_token] = nil

    redirect_to "/login.html?notice=Logout sucessfully"
  end

  def home
  end
end