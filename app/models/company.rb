class Company < ActiveRecord::Base
  validates :name, presence: true

  dragonfly_accessor :logo
end