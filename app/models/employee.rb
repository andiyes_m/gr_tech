class Employee < ActiveRecord::Base
  validates :first_name, presence: true
  validates :last_name, presence: true
  belongs_to :company

  def get_full_name
    self.first_name.to_s + " " + self.last_name.to_s
  end
end