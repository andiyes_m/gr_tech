class User < ActiveRecord::Base
  validates :email, presence: true, format: { with: proc {format_email} }
  validates :encrypted_password, presence: true

  def self.format_email
    /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i
  end
end