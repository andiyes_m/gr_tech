json.extract! company, :id, :id, :name, :email, :logo, :logo_uid, :logo_name, :website, :created_at, :updated_at
json.url company_url(company, format: :json)
