json.extract! employee, :id, :id, :first_name, :last_name, :company_id, :email, :phone, :created_at, :updated_at
json.url employee_url(employee, format: :json)
