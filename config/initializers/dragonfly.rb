require 'dragonfly'

# Configure
Dragonfly.app.configure do
  plugin :imagemagick

  secret "792865cb8b34b67085d1fdf6d9202b8714082e4511a3030eebcfb70e683ae152"

  url_format "/media/:job/:name"

  datastore :file,
    root_path: Rails.root.join('storage/app/public'),
    server_root: Rails.root.join('storage')
end

# Logger
Dragonfly.logger = Rails.logger

# Mount as middleware
Rails.application.middleware.use Dragonfly::Middleware

# Add model functionality
ActiveSupport.on_load(:active_record) do
  extend Dragonfly::Model
  extend Dragonfly::Model::Validations
end
