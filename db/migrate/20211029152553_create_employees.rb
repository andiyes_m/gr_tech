class CreateEmployees < ActiveRecord::Migration
  def change
    create_table :employees do |t|
      t.string :first_name, null: false
      t.string :last_name, null: false
      t.integer :company_id
      t.string :email
      t.string :phone

      t.timestamps null: false
    end
    add_foreign_key :employees, :companies
  end
end
